<?php

namespace App\Http\Controllers;

use App\Models\Annonce;
use App\Http\Requests\AnnonceRequest;
use Illuminate\Http\Request;

class AnnonceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $annonces=Annonce::all();
       return view("annonces.index", compact("annonces"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("annonces.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AnnonceRequest $request)
    {
        Annonce::create(
            [
                "titre"=>$request->titre,
                "description"=>$request->description,
                "type"=>$request->type,
                "neuf"=>$request->neuf,
                "ville"=>$request->ville,
                "superficie"=>$request->superficie,
                "prix"=>$request->prix,
            ]
        );
        return redirect()->route("annonce.index")->with("success", "la nouvelle annonce a été bien ajouté!");
    }

    /**
     * Display the specified resource.
     */
    public function show(Annonce $annonce)
    {
       return view("annonces.show", compact("annonce"));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Annonce $annonce)
    {
        return view("annonces.edit" , compact("annonce"));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Annonce $annonce)
    {
        $request->validate( [
         "titre"=> "required|unique:annonces,titre,".$annonce->id
        ]);

            $annonce->update($request->all());
         return redirect()->route("annonce.index")->with("success", "L'annonce a été mise à jour!");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Annonce $annonce)
    {
       $annonce->delete();
       return redirect()->route("annonce.index")->with("success", "L'annonce a été supprimé!");

    }
}
