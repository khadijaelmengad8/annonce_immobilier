@extends("templates.master")
@section("titre", "Liste des annonces")
@section("contenu")
<h2>Liste des annonces</h2>
<br>
    @if(session()->has("success"))
    <div class="alert alert-success">
        {{session("success")}}
    </div>
    @endif

<a href="{{ route('annonce.create') }}" class="btn btn-primary">Nouvelle annonce</a>
    @isset($annonces)
        <table class="table">
            <tr>
                <th>#</th>
                <th>Titre</th>
                <th>Description</th>
                <th>Type</th>
                <th>Ville</th>
                <th>Superficie (m<sup>2</sup>) </th>
                <th>Etat</th>
                <th>Prix</th>
                <th>Action</th>
            </tr>
            @foreach($annonces as $annonce)
            <tr>
                <td>{{ $annonce->id }}</td>
                <td>{{ $annonce->titre }}</td>
                <td>{{ $annonce->description }}</td>
                <td>{{ $annonce->type}}</td>
                <td>{{ $annonce->ville}}</td>
                <td>{{ $annonce->superficie}}</td>
                <td>{{ $annonce->neuf? "Neuf":"Ancien" }}</td>
                <td>{{ $annonce->prix }}</td>
                <td>
                    <form action="{{ route('annonce.destroy', $annonce->id )}}" method="post">
                         @csrf
                         @method("DELETE")
                        <a href="{{route('annonce.show', $annonce->id )}}"><i class="bi bi-eye"></i></a> 
                         <a href="{{route('annonce.edit', $annonce->id )}}"><i class="bi bi-pencil"></i></a> 
                        <button type="submit" style="border:none; background-color:transparent"
                        onclick="return confirm('Voulez vous supprimer l\'annonce :  {{ $annonce->titre }}?')"
                        
                        ><i class="bi bi-trash"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </table>
    @endisset
@endsection